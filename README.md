# Implementation for Wiley test task
There are several inerfaces in class hierarchy. Main implementation is in InMemoryCache (and subclasses LruCache, LfuCache), OnDiskCache and TwoLeveledCache which aggregates
first two caches.

In memory cache is based on HashMap and has two implementations based on he algorithm of elimenating the entries during cleanup of cache (LFU - Least Frequenly Used or LRU - Least Recently Used)).

OnDisk cache stores serialized entries like files (in the 'second_level_cache/' in directory (where executing program is located) and removes random entries during the cleanup.

Aggregator - TwoLeveledCache uses implementation of InMemoryCache and OnDiskCache. Main idea to store entries removed from first (in memory) level on second (disk) level.

Implementations are thread-safe by use of ReadWriteLock. Such way selected because attempt to implement lock-free version has been failed - the code was too difficult to understand and control correctness of it during concurrent execution.

# Tests
There are some unit and integration tests. Firsts cover only TwoLeveledCache class (keeping in mind that it is a test task), seconds demonstrate the use of TwoLeveledCache.

You can import project in Intelij Idea and run it using gradle. 