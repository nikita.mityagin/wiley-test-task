package com.wiley.testtask;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by camel on 28.03.17.
 */
public class TestUniqueObject implements Serializable {
   private final UUID uuid = UUID.randomUUID();

   @Override
   public String toString() {
       return "Object: " + uuid.toString();
   }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestUniqueObject that = (TestUniqueObject) o;

        return uuid.equals(that.uuid);
    }

    public UUID getUuid() {
       return uuid;
    }

}
