package com.wiley.testtask.implementation;

import com.wiley.testtask.TestUniqueObject;
import com.wiley.testtask.implementation.LruCache;
import com.wiley.testtask.implementation.OnDiskCache;
import com.wiley.testtask.implementation.TwoLeveledCache;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.UUID;

/**
 * Created by camel on 29.03.17.
 */
public class TwoLeveledCacheUnitTests {
    private LruCache inMemoryCache;
    private OnDiskCache onDiskCache;
    private TwoLeveledCache<UUID, TestUniqueObject> twoLeveledCache;
    private TestUniqueObject testUniqueObject;

    @Before
    public void initialize() {
        inMemoryCache = Mockito.mock(LruCache.class);
        onDiskCache = Mockito.mock(OnDiskCache.class);
        twoLeveledCache = new TwoLeveledCache<UUID, TestUniqueObject>(inMemoryCache, onDiskCache);
        testUniqueObject = new TestUniqueObject();
    }

    // put:
    @Test(expected = NullPointerException.class)
    public void put_NullKey_NullPointerException() {
        twoLeveledCache.put(null, testUniqueObject);
    }

    @Test(expected = NullPointerException.class)
    public void put_nullValue_NullPointerException() {
        twoLeveledCache.put(testUniqueObject.getUuid(), null);
    }

    @Test(expected = NullPointerException.class)
    public void put_nullKeyAndValue_NullPointerException() {
        twoLeveledCache.put(null, null);
    }

    @Test
    public void put_singleObject_inMemoryCachePutCalled() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        verify(inMemoryCache, times(1)).put(testUniqueObject.getUuid(), testUniqueObject);
    }

    @Test
    public void put_inMemoryCacheIsFull_inMemoryCacheCleanupCalled() {
        when(inMemoryCache.size()).thenReturn(2);
        when(inMemoryCache.getMaxSize()).thenReturn(2);

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        verify(inMemoryCache, times(1)).cleanup();
    }

    @Test
    public void put_notEnoughFreeSpaceInOnDiskCache_onDiskCacheCleanupCalled() {
        when(inMemoryCache.size()).thenReturn(1);
        when(inMemoryCache.getMaxSize()).thenReturn(1);
        when(onDiskCache.size()).thenReturn(1);
        when(onDiskCache.getMaxSize()).thenReturn(2);
        when(inMemoryCache.cleanup()).thenReturn(Collections.singletonMap(testUniqueObject.getUuid(), testUniqueObject));

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        verify(onDiskCache, times(1)).cleanup(1);
    }

    @Test
    public void put_inMemoryCacheIsFull_onDiskPutCalled() {
        when(inMemoryCache.size()).thenReturn(1);
        when(inMemoryCache.getMaxSize()).thenReturn(1);
        when(inMemoryCache.cleanup()).thenReturn(Collections.singletonMap(testUniqueObject.getUuid(), testUniqueObject));

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        verify(onDiskCache, times(1)).put(testUniqueObject.getUuid(), testUniqueObject);
    }

    // get:
    @Test(expected = NullPointerException.class)
    public void get_nullKey_NUllPointerException() {
        twoLeveledCache.get(null);
    }

    @Test
    public void get_eachState_inMemoryGetCalled() {
        twoLeveledCache.get(testUniqueObject.getUuid());

        verify(inMemoryCache, times(1)).get(testUniqueObject.getUuid());
    }

    @Test
    public void get_entryIsInMemory_returnValueFromInMemoryCache() {
        when(inMemoryCache.get(testUniqueObject.getUuid())).thenReturn(testUniqueObject);

        TestUniqueObject value = twoLeveledCache.get(testUniqueObject.getUuid());

        Assert.assertEquals(testUniqueObject, value);
    }

    @Test
    public void get_entryIsOnDisk_returnValueFromDisk() {
        when(inMemoryCache.get(testUniqueObject.getUuid())).thenReturn(null);
        when(onDiskCache.get(testUniqueObject.getUuid())).thenReturn(testUniqueObject);

        TestUniqueObject value = twoLeveledCache.get(testUniqueObject.getUuid());

        Assert.assertEquals(testUniqueObject, value);
    }

    @Test
    public void get_entryIsOnDisk_entryInsertedInInMemoryCache() {
        when(inMemoryCache.get(testUniqueObject.getUuid())).thenReturn(null);
        when(onDiskCache.get(testUniqueObject.getUuid())).thenReturn(testUniqueObject);

        TestUniqueObject value = twoLeveledCache.get(testUniqueObject.getUuid());

        verify(inMemoryCache, times(1)).put(value.getUuid(), value);
    }

    @Test
    public void get_entryIsAbsent_returnNull() {
        when(inMemoryCache.get(testUniqueObject.getUuid())).thenReturn(null);
        when(onDiskCache.get(testUniqueObject.getUuid())).thenReturn(null);

        TestUniqueObject value = twoLeveledCache.get(testUniqueObject.getUuid());

        Assert.assertTrue(value == null);
    }

    // clear:
    @Test
    public void clear_eachState_inMemoryClearAndOnDiskClearCalled() {
        twoLeveledCache.clear();

        verify(inMemoryCache, times(1)).clear();
        verify(onDiskCache, times(1)).clear();
    }

    // size:
    @Test
    public void size_eachState_inMemorySizeAndOnDiskSizeCalled() {
        when(inMemoryCache.size()).thenReturn(1);
        when(onDiskCache.size()).thenReturn(0);

        twoLeveledCache.size();

        verify(inMemoryCache, times(1)).size();
        verify(onDiskCache, times(1)).size();
    }

    @Test
    public void size_oneObjectInMemoryAndOneOnDisk_sizeIsSumOfSizeOfInMemoryAndOnDisk() {
        when(inMemoryCache.size()).thenReturn(1);
        when(onDiskCache.size()).thenReturn(1);

        int size = twoLeveledCache.size();

        Assert.assertTrue(size == 2);
    }

}
