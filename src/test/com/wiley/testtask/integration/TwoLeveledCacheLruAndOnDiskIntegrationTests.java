package com.wiley.testtask.integration;

import com.wiley.testtask.TestUniqueObject;
import com.wiley.testtask.api.ITwoLeveledCacheBuilder;
import com.wiley.testtask.implementation.TwoLeveledCache;
import com.wiley.testtask.implementation.TwoLeveledCacheBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

/**
 * Created by camel on 29.03.17.
 */
public class TwoLeveledCacheLruAndOnDiskIntegrationTests {
    private TwoLeveledCache<UUID, TestUniqueObject> twoLeveledCache;
    private TestUniqueObject testUniqueObject;

    @Before
    public void initialize() {
        ITwoLeveledCacheBuilder<UUID,com.wiley.testtask.TestUniqueObject> builder =
                new TwoLeveledCacheBuilder<UUID, TestUniqueObject>();
        twoLeveledCache = builder
               .maxSizeOfFirstLevel(2)
               .maxSizeOfSecondLevel(2)
               .strategy(ITwoLeveledCacheBuilder.Strategy.LRU)
               .build();

        testUniqueObject = new TestUniqueObject();
    }

    @Test
    public void put_singleEntry_getReturnsPutEntry() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        Assert.assertEquals(testUniqueObject, twoLeveledCache.get(testUniqueObject.getUuid()));
    }

    @Test
    public void put_singleEntry_sizeIs1() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        Assert.assertEquals(testUniqueObject, twoLeveledCache.get(testUniqueObject.getUuid()));
    }

    @Test
    public void put_entryWhichAlreadyInCache_sizeIs1() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        Assert.assertTrue(twoLeveledCache.size() == 1);
    }

    @Test
    public void put_entryWhichAlreadyInCache_getReturnsPutEntry() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        Assert.assertEquals(testUniqueObject, twoLeveledCache.get(testUniqueObject.getUuid()));
    }

    @Test
    public void put_entriesCountLessThenMaxSizeOfFirstLevel_sizeEqualsToPutCount() {
        TestUniqueObject oneMoreObject = new TestUniqueObject();

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(oneMoreObject.getUuid(), oneMoreObject);

        Assert.assertTrue(twoLeveledCache.size() == 2);
    }

    @Test
    public void put_entriesCountLessThenMaxSizeOfFirstLevel_getReturnsPutObject() {
        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);

        Assert.assertEquals(testUniqueObject, twoLeveledCache.get(testUniqueObject.getUuid()));
    }

    @Test
    public void put_entriesCountEqualsToMaxSizeOfFirstLevel_sizeEqualsToPutCount() {
        TestUniqueObject oneMoreObject = new TestUniqueObject();

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(oneMoreObject.getUuid(), oneMoreObject);

        Assert.assertTrue(twoLeveledCache.size() == 2);
    }

    @Test
    public void put_entriesCountEqualsToMaxSizeOfFirstLevel_allPutObjectAreInCache() {
        TestUniqueObject oneMoreObject = new TestUniqueObject();

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(oneMoreObject.getUuid(), oneMoreObject);

        Assert.assertEquals(testUniqueObject, twoLeveledCache.get(testUniqueObject.getUuid()));
        Assert.assertEquals(oneMoreObject, twoLeveledCache.get(oneMoreObject.getUuid()));
    }

    @Test
    public void put_entriesCountEqualsOrMoreThenMaxSizeOfFirstLevelAndLessThenSummaryCacheCapacity_sizeLessOrEqualsToPutCount() {
        TestUniqueObject oneMoreObject1 = new TestUniqueObject();
        TestUniqueObject oneMoreObject2 = new TestUniqueObject();

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(oneMoreObject1.getUuid(), oneMoreObject1);
        twoLeveledCache.put(oneMoreObject2.getUuid(), oneMoreObject2);

        Assert.assertTrue(twoLeveledCache.size() <= 3);
    }

    @Test
    public void put_entriesCountEqualsOrMoreThenSummaryCacheCapacity_sizeLessThenSummaryCacheCapacity() {
        TestUniqueObject oneMoreObject1 = new TestUniqueObject();
        TestUniqueObject oneMoreObject2 = new TestUniqueObject();
        TestUniqueObject oneMoreObject3 = new TestUniqueObject();

        twoLeveledCache.put(testUniqueObject.getUuid(), testUniqueObject);
        twoLeveledCache.put(oneMoreObject1.getUuid(), oneMoreObject1);
        twoLeveledCache.put(oneMoreObject2.getUuid(), oneMoreObject2);
        twoLeveledCache.put(oneMoreObject3.getUuid(), oneMoreObject3);

        Assert.assertTrue(twoLeveledCache.size() < 4);
    }

}
