package com.wiley.testtask.implementation;

import com.wiley.testtask.api.Cache;
import com.wiley.testtask.api.RestrictedSizeCache;
import com.wiley.testtask.utils.CacheFilesUtils;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by camel on 28.03.17.
 */

/**
 * On disk cache. Stores serialized entries in DIRECTORY. Random entries are removed during the cleanup.
 * Implementation is thread-safe.
 */
public class OnDiskCache<K extends Serializable, V extends Serializable> implements Cache<K, V>, RestrictedSizeCache {
    private final String DIRECTORY = "second_level_cache/"; // directory to save serialized objects (second level of caching)
    private final File cacheDirectory = new File(DIRECTORY);
    private final int maxSize;
    private Integer countOfElements = new Integer(0);
    private final double maxLoadFactor = 0.9;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final CacheFilesUtils<K, V> cacheFilesUtils = new CacheFilesUtils<K, V>();

    OnDiskCache(int maxSize) {
        this.maxSize = maxSize;

        cleanDirectory(cacheDirectory);

        cacheDirectory.mkdir();
    }

    private void cleanDirectory(File directory) {
        File[] oldData = directory.listFiles();
        for (int i = 0; i < oldData.length; ++i) {
            if (oldData[i].isDirectory()) {
                cleanDirectory(oldData[i]);
            } else {
                oldData[i].delete();
            }
        }

        directory.delete();
    }

    /**
     * Puts value in cache by given key. Saves key and value on disk in cache's directory.
     *
     * @param key - key of value
     * @param value - value given by key
     */
    @Override
    public void put(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Key and value can't be null.");
        }

        readWriteLock.writeLock().lock();

        String pathToBucket = getPathToBasket(key);
        File file = new File(pathToBucket);
        if (!file.exists()) {
            file.mkdirs();
        }

        file = new File(pathToBucket + file.listFiles().length); // usually only one file per basket

        if (cacheFilesUtils.saveKeyAndValueOnDisk(key, value, file)) {
            ++countOfElements;
        }

        readWriteLock.writeLock().unlock();
    }

    @Override
    public V get(K key) {
        if (key == null) {
            throw new NullPointerException("Key can't be null.");
        }

        readWriteLock.readLock().lock();

        File[] files = new File(getPathToBasket(key)).listFiles();
        V value = null;

        for (File file: files) {
            // TODO: auto removable now, just for test task, but better to extract the logic of deletion
            V temporaryValue = cacheFilesUtils.getValueAndRemoveFileIfKeyInside(key, file);

            if (temporaryValue != null) {
                value = temporaryValue;
            }
        }

        readWriteLock.readLock().unlock();

        return value;
    }

    /**
     * Removes at least countToRemoveAtLeast entries from cache (or all if given parameter more then current size of
     * cache). If there are more entries then maxLoadFactor allows, cache will be cleaned to provide capacity needed
     * by maxLoadFactor (even if the number more then countToRemoveAtLeast).
     *
     * @param countToRemoveAtLeast - min amount of entries to be removed
     */
    public void cleanup(int countToRemoveAtLeast) {
        if (readWriteLock.writeLock().tryLock()) {
            try {
                int deltaForCleanup = 0;
                int deltaForCleanupByLoadFactor = countOfElements - (int) (maxSize * maxLoadFactor);

                if (countOfElements < countToRemoveAtLeast) {
                    deltaForCleanup = countOfElements;
                } else if (countToRemoveAtLeast < 0 || countToRemoveAtLeast < deltaForCleanupByLoadFactor) {
                    deltaForCleanup = deltaForCleanupByLoadFactor;
                } else {
                    deltaForCleanup = countToRemoveAtLeast;
                }

                if (deltaForCleanup > 0) {
                    File[] baskets = cacheDirectory.listFiles();

                    while (deltaForCleanup > 0 && baskets.length > 0) {
                        File[] elements = baskets[deltaForCleanup - 1].listFiles();
                        for (int i = 0; i < elements.length; ++i) {
                            elements[i].delete();
                        }

                        baskets[deltaForCleanup - 1].delete();

                        --deltaForCleanup;
                        --countOfElements;
                    }
                }

                if (countOfElements < 0) {
                    throw new IllegalStateException("Corrupted state of OnDiskCache.");
                }
            } finally {
                readWriteLock.writeLock().unlock();
            }
        }
    }

    private String getPathToBasket(K key) {
        return DIRECTORY + String.valueOf(key.hashCode()) + "/";
    }

    @Override
    public void clear() {
        readWriteLock.writeLock().lock();

        for (File basket: cacheDirectory.listFiles()) {
            for (File file: basket.listFiles()) {
                file.delete();
            }
        }

        readWriteLock.writeLock().unlock();
    }

    @Override
    public int size() {
        return countOfElements.intValue();
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }
}

