package com.wiley.testtask.implementation;

import java.util.*;

/**
 * Created by camel on 28.03.17.
 */

/**
 * In Memory Cache using Least Frequently Used strategy to eliminate elements during cleanup.
 */
public class LfuCache<K, V> extends InMemoryCache<K, V> {

    private final Map<K, Integer> frequencies = new HashMap<K, Integer>();

    LfuCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected void doStrategyLogicOnPut(K key, V value) {
        frequencies.put(key, new Integer(0));
    }

    @Override
    protected void doStrategyLogicOnRemoveEntry(K key, V value) {
        frequencies.remove(key);
    }

    @Override
    protected void doStrategyLogicOnGet(K key, V value) {
        Integer valueFrequency = frequencies.get(key);
        if (valueFrequency != null) {
            ++valueFrequency;
        }
    }

    @Override
    protected List<K> calculateKeysOfElementsToBeRemoved(int countToRemove) {
        if (countToRemove > 0) {
            List<Map.Entry<K, Integer>> sortedEntries =
                    new ArrayList<Map.Entry<K,Integer>>(frequencies.entrySet());

            Collections.sort(sortedEntries, (o1, o2) -> {
                int intO1 = o1.getValue().intValue();
                int intO2 = o2.getValue().intValue();
                return intO1 > intO2 ? 1 : intO1 < intO2 ? 1 : 0;
            });

            List<K> keysToBeRemoved = new ArrayList<K>(countToRemove);
            for (int i = 0; i < countToRemove; ++i) {
                keysToBeRemoved.add(sortedEntries.get(i).getKey());
            }

            return keysToBeRemoved;
        }

        return Collections.emptyList();
    }

}
