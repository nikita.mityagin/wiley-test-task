package com.wiley.testtask.implementation;

import com.wiley.testtask.api.ITwoLeveledCacheBuilder;

import java.io.Serializable;

/**
 * Created by camel on 19.03.17.
 */
public class TwoLeveledCacheBuilder<K extends Serializable, V extends Serializable> implements ITwoLeveledCacheBuilder<K, V> {
    private Strategy strategy = Strategy.LFU;
    private int maxSizeOfFirstLevel = 512;
    private int maxSizeOfSecondLevel = 2048;

    @Override
    public ITwoLeveledCacheBuilder<K,V> strategy(Strategy strategy) {
        this.strategy = strategy;

        return this;
    }

    @Override
    public ITwoLeveledCacheBuilder<K,V> maxSizeOfFirstLevel(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("Size of first level can't be less then 1.");
        }

        this.maxSizeOfFirstLevel = size;

        return this;
    }

    @Override
    public ITwoLeveledCacheBuilder<K,V> maxSizeOfSecondLevel(int size) {
        if (size < 0) {
            throw new IllegalArgumentException("Size of second level can't be less then 0.");
        }

        this.maxSizeOfSecondLevel = size;

        return this;
    }

    @Override
    public TwoLeveledCache<K, V> build() {
        switch (strategy) {
            case LRU:
                return new TwoLeveledCache<K, V>(new LruCache<K, V>(maxSizeOfFirstLevel), new OnDiskCache<K, V>(maxSizeOfSecondLevel));
            case LFU:
            default:
                return new TwoLeveledCache<K, V>(new LfuCache<K, V>(maxSizeOfFirstLevel), new OnDiskCache<K, V>(maxSizeOfSecondLevel));
        }
    }

}
