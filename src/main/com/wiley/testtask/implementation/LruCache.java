package com.wiley.testtask.implementation;

import java.util.*;

/**
 * Created by camel on 28.03.17.
 */

/**
 * In Memory Cache using Least Recently Used strategy to eliminate elements during cleanup.
 */
public class LruCache<K, V> extends InMemoryCache<K, V> {
    private final Queue<K> accessQueue = new LinkedList<K>();

    LruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected void doStrategyLogicOnPut(K key, V value) {
        removeAndAddKeyToTheQueue(key);
    }

    private void removeAndAddKeyToTheQueue(K key) {
        removeAllSameKeys(key);
        accessQueue.add(key);
    }

    private void removeAllSameKeys(K key) {
        while (accessQueue.contains(key)) {
            accessQueue.remove(key);
        }
    }

    @Override
    protected void doStrategyLogicOnRemoveEntry(K key, V value) {
        removeAllSameKeys(key);
    }

    @Override
    protected void doStrategyLogicOnGet(K key, V value) {
        removeAndAddKeyToTheQueue(key);
    }

    @Override
    protected List<K> calculateKeysOfElementsToBeRemoved(int countToRemove) {
        if (countToRemove > 0) {
            List<K> keysToBeRemoved = new ArrayList<K>(countToRemove);

            for (int i = 0; i < countToRemove; ++i) {
                K key = accessQueue.poll();
                if (key != null) {
                    keysToBeRemoved.add(key);
                } else {
                    break; // the queue is empty
                }
            }

            return keysToBeRemoved;
        }

        return Collections.emptyList();
    }

}

