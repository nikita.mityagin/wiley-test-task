package com.wiley.testtask.implementation;

import com.wiley.testtask.api.CacheCleanedByStrategy;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by camel on 28.03.17.
 */

/**
 * Thread-safe implementation of in memory cache. Allows to add strategy logic for entries to be eliminated during
 * cleanup.
 */
public abstract class InMemoryCache<K, V> extends CacheCleanedByStrategy<K, V> {
    private final Map<K, SoftReference<V>> inMemoryCache = new HashMap<K, SoftReference<V>>();
    protected final int maxSize;
    private final double maxLoadFactor = 0.9;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    InMemoryCache(int maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * Puts value in map by given key.
     * @param key - key for map
     * @param value - value to be stored by SoftReference
     */
    @Override
    public void put(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Key and value can't be null.");
        }

        readWriteLock.writeLock().lock();

        inMemoryCache.put(key, new SoftReference<V>(value));

        doStrategyLogicOnPut(key, value);

//        if (inMemoryCache.size() >= maxSize) {
//            cleanup();
//        }

        readWriteLock.writeLock().unlock();
    }

    /**
     * Returns Object stored by the key.
     *
     * SoftReference allow GC to remove containing object and the reference itself. So null will be returned.
     *
     * @param key - key to retrieve the cached object
     * @return object which earlier has been put by the given key or null if there are no object by the given key
     */
    @Override
    public V get(K key) {
        if (key == null) {
            throw new NullPointerException("Key can't be null.");
        }

        readWriteLock.readLock().lock();

        final SoftReference<V> softReferenceToValue = inMemoryCache.get(key);

        V value = null;
        if (softReferenceToValue != null && softReferenceToValue.get() != null) {
            value = softReferenceToValue.get();
            doStrategyLogicOnGet(key, value);
        }

        readWriteLock.readLock().unlock();

        return value;
    }

    /**
     * The method uses same write lock as put method, so it is needed because putting element in cache during cleanup
     * can corrupt state of the cache, cause memory leaks, undefined behaviour. Also try lock used because if cleanup
     * is always called from put method and if currently lock is busy it means that some thread putting the element and
     * will call cleanup soon again.
     * Such code can be done lock-free, but to simplify implementation it uses only ReadWriteLock.
     * @return map of removed elements
     */
    public Map<K, V> cleanup() {
        if (readWriteLock.writeLock().tryLock()) { // only one thread will perform cleanup, others skip the operation
            try {
                Map<K, V> removedEntries = new HashMap<K, V>();
                List<K> keysToBeRemoved = calculateKeysOfElementsToBeRemoved(
                        inMemoryCache.size() - (int) (maxLoadFactor * maxSize)
                );

                for (K key: keysToBeRemoved) {
                    SoftReference<V> referenceToValue = inMemoryCache.remove(key);

                    if (referenceToValue != null && referenceToValue.get() != null) {
                        V value = referenceToValue.get();

                        removedEntries.put(key, value);

                        doStrategyLogicOnRemoveEntry(key, value);
                    }

                }

                return removedEntries;
            } finally {
                readWriteLock.writeLock().unlock();
            }
        }

        return Collections.emptyMap();
    }

    @Override
    public void clear() {
        readWriteLock.writeLock().lock();

        inMemoryCache.clear();

        readWriteLock.writeLock().unlock();
    }

    @Override
    public int size() {
        return inMemoryCache.size();
    }

    @Override
    public int getMaxSize() {
        return maxSize;
    }
}
