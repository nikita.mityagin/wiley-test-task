package com.wiley.testtask.implementation.markers;

/**
 * Created by camel on 29.03.17.
 */

/**
 * Marker to show that Cache implementation supports cleanup but only manually
 */
public interface ManuallyCleanedUpCache {
}
