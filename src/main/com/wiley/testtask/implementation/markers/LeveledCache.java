package com.wiley.testtask.implementation.markers;

import com.wiley.testtask.api.Cache;

/**
 * Created by camel on 19.03.17.
 */

/**
 * Marker to show that Cache implementation supports several levels
 */
public interface LeveledCache<K, V> extends Cache<K, V> {
}
