package com.wiley.testtask.implementation;

import com.wiley.testtask.implementation.markers.LeveledCache;
import com.wiley.testtask.implementation.markers.ManuallyCleanedUpCache;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by camel on 19.03.17.
 */
public class TwoLeveledCache<K extends Serializable, V extends Serializable>
        implements LeveledCache<K, V>, ManuallyCleanedUpCache {

    private final InMemoryCache<K, V> inMemoryCache;
    private final OnDiskCache<K, V> onDiskCache;
    private final Lock pushDownLock = new ReentrantLock();

    /**
     * Protected to allow creation only by use of TwoLeveledCacheBuilder.
     *
     * @param inMemoryCache - configured inMemory cache to be used as first level
     * @param onDiskCache - configured onDisk cache to be used as second level
     */
    TwoLeveledCache(InMemoryCache<K, V> inMemoryCache, OnDiskCache<K, V> onDiskCache) {
        this.inMemoryCache = inMemoryCache;
        this.onDiskCache = onDiskCache;
    }

    /**
     * Puts value in map by given key. Also tries to perform cleanup of first level and push removed elements to second
     * level.
     *
     * @param key - key for map
     * @param value - value to be stored in cache by given key
     */
    @Override
    public void put(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Key and value can't be null.");
        }

        inMemoryCache.put(key, value);

        tryPushDown();
    }

    private void tryPushDown() {
        if (inMemoryCache.size() >= inMemoryCache.getMaxSize() && pushDownLock.tryLock()) {
            try {
                Map<K, V> removed = inMemoryCache.cleanup();

                if (onDiskCache.size() + removed.size() >= onDiskCache.getMaxSize()) {
                    onDiskCache.cleanup(onDiskCache.size() + removed.size() + 1 - onDiskCache.getMaxSize());
                }

                for (Map.Entry<K, V> entry: removed.entrySet()) {
                    onDiskCache.put(entry.getKey(), entry.getValue());
                }
            } finally {
                pushDownLock.unlock();
            }
        }
    }

    /**
     * Returns Value stored by the key. If it is stored on disk level then puts it on in-Memory level.
     *
     * @param key - key to retrieve the cached value
     * @return value which earlier has been put by the given key or null if there are no value by the given key
     */
    @Override
    public V get(K key) {
        if (key == null) {
            throw new NullPointerException("Key can't be null.");
        }

        V value = inMemoryCache.get(key);

        if (value == null) {
            value = onDiskCache.get(key);

            if (value != null) {
                inMemoryCache.put(key, value); // repair from second level
            }
        }

        return value;
    }

    /**
     * Clears both levels.
     */
    @Override
    public void clear() {
        inMemoryCache.clear();
        onDiskCache.clear();
    }

    /**
     * Calculates current size of cache.
     *
     * @return - current size of cache
     */
    @Override
    public int size() {
        return inMemoryCache.size() + onDiskCache.size();
    }
}


