package com.wiley.testtask.utils;

import java.io.*;

/**
 * Created by camel on 19.03.17.
 */
public class CacheFilesUtils<K extends Serializable, V extends Serializable> {

    public boolean saveKeyAndValueOnDisk(K key, V value, File file) {
        OutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(file));
            objectOutputStream = new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(key);
            objectOutputStream.writeObject(value);
            objectOutputStream.flush();
        } catch (FileNotFoundException e) {
            System.out.println(
                    "File not found when saving object on disk: " + file.getAbsolutePath() + file.getName() + ", " +
                    e.getMessage()
            );
            return false;
        } catch (IOException e) {
            System.out.println(
                    "There is problem while saving object on disk: " + file.getAbsolutePath() + file.getName() + ", " +
                    e.getMessage()
            );
            return false;
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                System.out.println(
                        "There is problem closing output stream when working with file: " + file.getAbsolutePath() +
                        file.getName() + ", " + e.getMessage()
                );
            }
        }

        return true;
    }

    public V getValueAndRemoveFileIfKeyInside(K key, File file) {
        InputStream inputStream = null;
        ObjectInputStream objectInputStream = null;

        try {
            inputStream = new BufferedInputStream(new FileInputStream(file));
            objectInputStream = new ObjectInputStream(inputStream);

            K keyFromFile = (K) objectInputStream.readObject();

            if (key.equals(keyFromFile)) {
                V value = (V) objectInputStream.readObject();
                file.delete();
                return value;
            }
        } catch (FileNotFoundException e) {
            System.out.println(
                    "File not found when reading objects from disk: " + file.getAbsolutePath() + file.getName() + ", " +
                    e.getMessage()
            );
        } catch (IOException e) {
            System.out.println(
                    "There is problem while reading objects from disk: " + file.getAbsolutePath() + file.getName() +
                    ", " + e.getMessage()
            );
        } catch (ClassNotFoundException e) {
            System.out.println("There is problem while converting objects have been red from disk: " + e.getMessage());
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                System.out.println(
                        "There is problem closing input stream when working with file: " + file.getAbsolutePath() +
                        file.getName()+ ", " + e.getMessage()
                );
            }
        }

        return null;
    }

}
