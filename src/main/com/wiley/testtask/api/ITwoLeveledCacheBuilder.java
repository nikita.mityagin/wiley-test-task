package com.wiley.testtask.api;

import com.wiley.testtask.implementation.TwoLeveledCache;

import java.io.Serializable;

/**
 * Created by camel on 31.03.17.
 */
public interface ITwoLeveledCacheBuilder<K extends Serializable, V extends Serializable> {

    /**
     * Sets strategy for entries elimination from first level
     * @param strategy - strategy for entries elimination from first level
     * @return - current builder instance
     */
    ITwoLeveledCacheBuilder<K,V> strategy(Strategy strategy);

    /**
     * Sets max size of first level
     * @param size - max size of first level
     * @return - current builder instance
     */
    ITwoLeveledCacheBuilder<K,V> maxSizeOfFirstLevel(int size);

    /**
     * Sets max size of second level
     * @param size - max size of second level
     * @return - current builder instance
     */
    ITwoLeveledCacheBuilder<K,V> maxSizeOfSecondLevel(int size);

    /**
     * Creates cache instance using parameters specified on builder
     * @return
     */
    TwoLeveledCache<K, V> build();

    public static enum Strategy {
        LRU,
        LFU
    }
}
