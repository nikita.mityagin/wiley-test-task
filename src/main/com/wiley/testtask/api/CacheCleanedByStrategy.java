package com.wiley.testtask.api;

import java.util.List;

/**
 * Created by camel on 28.03.17.
 */
public abstract class CacheCleanedByStrategy<K, V> implements Cache<K, V>, RestrictedSizeCache {
    /**
     * Performs strategy-specific logic when putting the element.
     *
     * @param key - key of putting element
     * @param value - value of putting element
     */
    protected abstract void doStrategyLogicOnPut(K key, V value);

    /**
     * Performs strategy-specific logic when getting the element.
     *
     * @param key - key of caching element
     * @param value - value of caching element
     */
    protected abstract void doStrategyLogicOnGet(K key, V value);

    /**
     * Performs strategy-specific logic when removing the element.
     *
     * @param key - key of caching element
     * @param value - value of caching element
     */
    protected abstract void doStrategyLogicOnRemoveEntry(K key, V value);

    /**
     * @return List of Keys to remove elements by them from cache
     */
    protected abstract List<K> calculateKeysOfElementsToBeRemoved(int countToRemove);
}
