package com.wiley.testtask.api;

/**
 * Created by camel on 28.03.17.
 */
public interface RestrictedSizeCache {
    int getMaxSize();
}
