package com.wiley.testtask.api;

/**
 * Created by camel on 19.03.17.
 */
public interface Cache<K, V> {
    /**
     * Puts value in map by given key.
     * @param key - key for map
     * @param value - value to be stored by SoftReference
     */
    void put(K key, V value);

    /**
     * Returns value stored by the key.
     *
     * @param key - key to retrieve the cached value
     * @return value which earlier has been put by the given key or null if there are no value by the given key
     */
    V get(K key);

    /**
     * Removes all entries from cache.
     */
    void clear();

    /**
     * Calculates count of entries in cache.
     * @return count of entries in cache
     */
    int size();
}
